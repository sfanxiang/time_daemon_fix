#include <fcntl.h>
#include <linux/rtc.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

/* Number of days per month (except for February in leap years). */
static const int monoff[] = {
	0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334
};

static int is_leap_year(int year)
{
	return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
}

static int leap_days(int y1, int y2)
{
	--y1;
	--y2;
	return (y2/4 - y1/4) - (y2/100 - y1/100) + (y2/400 - y1/400);
}

/*
 * Code adapted from Python 2.4.1 sources (Lib/calendar.py).
 */
time_t timegm2(const struct tm *tm)
{
	int year;
	time_t days;
	time_t hours;
	time_t minutes;
	time_t seconds;

	year = 1900 + tm->tm_year;
	days = 365 * (year - 1970) + leap_days(1970, year);
	days += monoff[tm->tm_mon];

	if (tm->tm_mon > 1 && is_leap_year(year))
		++days;
	days += tm->tm_mday - 1;

	hours = days * 24 + tm->tm_hour;
	minutes = hours * 60 + tm->tm_min;
	seconds = minutes * 60 + tm->tm_sec;

	return seconds;
}

time_t get_rtc_time(const char *file)
{
	int fd;
	struct rtc_time rtc_tm;
	int ret;

	fd = open(file, O_RDONLY, 0);
	if(fd == -1) return (time_t)-1;

	ret = ioctl(fd, RTC_RD_TIME, &rtc_tm);
	close(fd);

	if(ret != 0) return (time_t)-1;

	if(sizeof(struct rtc_time) == sizeof(struct tm)) {
		return timegm2((struct tm*)&rtc_tm);
	} else {
		struct tm t;
		t.tm_sec = rtc_tm.tm_sec;
		t.tm_min = rtc_tm.tm_min;
		t.tm_hour = rtc_tm.tm_hour;
		t.tm_mday = rtc_tm.tm_mday;
		t.tm_mon = rtc_tm.tm_mon;
		t.tm_year = rtc_tm.tm_year;
		t.tm_wday = rtc_tm.tm_wday;
		t.tm_yday = rtc_tm.tm_yday;
		t.tm_isdst = rtc_tm.tm_isdst;
		return timegm2(&t);
	}
}

time_t get_system_time()
{
	return time(NULL);
}

#define RTC_FILE "/dev/rtc0"
#define WRITE_FILE "/data/time/ats_1"

void main()
{
	do {
		time_t rtc_time = get_rtc_time(RTC_FILE);
		if(rtc_time == -1) {
			perror("getting rtc time");
			continue;
		}
		time_t system_time = get_system_time();
		if(system_time == -1) {
			perror("getting system time");
			continue;
		}

		// printf("%ld %ld\n", rtc_time, system_time);

		long long offset = ((long long)system_time - rtc_time) * 1000;

		int fd = open(WRITE_FILE, O_WRONLY | O_CREAT, 0644);
		if(fd == -1) {
			perror("opening \"" WRITE_FILE "\"");
			continue;
		}

		if(write(fd, &offset, sizeof(offset)) == -1) {
			perror("writing to \"" WRITE_FILE "\"");
		}

		close(fd);
	} while((sleep(10), 1));
}
