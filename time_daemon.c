#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main(int argc, char *argv[])
{
	if(argc > 1) {
		if(strcmp(argv[1], "--no-orig") != 0) {
			system("time_daemon_orig &");
		}
	}
	system("time_daemon_fix &");
}
